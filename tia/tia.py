'''
Implements a driver for detectors that use the TIA COM interface.
The tricky bit is that TIA doesn't distinguish between the reset, expose, and readout
steps of using a CCD detector.  It only includes an all-encompassing aquire method.
This means one needs to wait for the complete acquisition to finish, including multi-second 
readout.  To be able to do other things in hardware while the detector is reading out
this driver fakes seperate exposure and readout stages by running the acquisition in a
seperate thread, and returning after the calculated exposure time.  Later readouts then block
on the acquisition thread to finish.

Todo: currently only developed against single detector TIA setups.
'''

import time
import numpy
import threading

import comtypes
import comtypes.client as cc
import comtypes.gen.ESVision as esv

import errors
import constants

class Driver(object):

	_esv    = esv
	_image  = None
	_thread = None
	_name   = "python-com-driver"

	def __init__(self):
		self._reset()
		self._start()
		self._link()
	
	def _reset(self):
		
		self._com = cc.CreateObject('ESVision.Application')
		self._ccd = self._com.CcdServer().QueryInterface(esv.ICcdServer)
		self._acq = self._com.AcquisitionManager().QueryInterface(esv.IAcquisitionManager)
		
		# reset TIA display window for the driver
		if self._com.FindDisplayWindow(self._name):
			self._com.CloseDisplayWindow(self._name)
		self._window = self._com.AddDisplayWindow().QueryInterface(esv.IDisplayWindow)
		self._window.Name = self._name
		self._display = self._window.AddDisplay(self._name, 0, 0, 0, 1).QueryInterface(esv.IImageDisplay)
		
		# reset CCD setup
		# requires an active TIA window
		if self._acq.DoesSetupExist(self._name):
			self._acq.DeleteSetup(self._name)
		self._acq.AddSetup(self._name)

		# these are default constant settings 
		self._select()
		self._ccd.AcquireMode = constants.AcquisitionMode.Single
		self._ccd.AcquisitionType = constants.AcquisitionType.Normal
		self._ccd.SeriesSize  = 1

	def _select(self):
		self._com.ActivateDisplayWindow(self._name)
		self._acq.SelectSetup(self._name)
		
	def _link(self):
		self._exposed.clear()
		if self._image:
			self._display.DeleteObject(self._image)
			self._image = None
		cal = self._com.Calibration2D(0,0,1,1,0,0)
		rows, cols = self._get_readout_dims()
		self._image = self._display.AddImage(self._name, cols, rows, cal).QueryInterface(esv.IImage)
		self._acq.LinkSignal(self._ccd.Camera, self._image)
		
	def set(self, **settings):
		t0 = time.time()
		with self._pending:
			self._select()
			self._set_binning(settings.get('binning', 1))
			self._set_cropping(settings.get('cropping', self._get_cropping_limits()))
			self._set_exposure_time(settings.get('exposure_time', 1.0))
			self._link()

	def _start(self):
		self._trigger = threading.Lock()
		self._pending = threading.Lock()
		self._readout = threading.Lock()
		self._started = threading.Event()
		self._exposed = threading.Event()
		def acquire():
			t0 = time.time()
			comtypes.CoInitialize()
			com = cc.CreateObject('ESVision.Application')
			acq = com.AcquisitionManager()
			self._trigger.acquire()
			while True:
				self._trigger.acquire()
				with self._pending:
					self._started.set()
					acq.Acquire()
					time.sleep(6)
					self._exposed.set()
		self._thread = threading.Thread(target=acquire)
		self._thread.daemon = True
		self._thread.start()

	def expose(self):
		# we are starting a new exposure
		# so clear these events
		self._started.clear()
		self._exposed.clear()
		# release the acquire loop for one cycle
		self._trigger.release()
		# wait until the acquire loop tells us it actually started
		self._started.wait()
		# now sleep until the exposure part of the acquition is done.
		# we need to calculate how long this is likely to be + a small
		# safety buffer
		buffer = self._get_exposure_time_limits()[0]
		expose_time = self._get_exposure_time()
		time.sleep(expose_time + buffer)

	def get(self):
		return {
			'binning': self._get_binning(),
			'cropping': self._get_cropping(),
			'exposure_time': self._get_exposure_time()
		}

	def readout(self):
		# wait until there is an exposed image
		self._exposed.wait()
		# convert image data to numpy array
		image = None
		if self._image:
			data = self._image.Data
			rows = data.PixelsY
			cols = data.PixelsX
			image = numpy.array(data.Array).reshape((rows, cols))
		return image
	
	def _get_binning(self):
		return self._ccd.Binning
	
	def _set_binning(self, binning):
		self._ccd.Binning = binning

	def _get_readout_dims(self):
		readout = self._ccd.PixelReadoutRange
		return readout.SizeY, readout.SizeX

	def _get_cropping(self):
		readout = self._ccd.PixelReadoutRange
		return (readout.StartY, readout.EndY), (readout.StartX, readout.EndX)
	
	def _set_cropping(self, cropping):
		readout = self._ccd.PixelReadoutRange
		readout.StartY = cropping[0][0]
		readout.StartX = cropping[1][0]
		readout.EndY   = cropping[0][1]
		readout.EndX   = cropping[1][1]
		self._ccd.PixelReadoutRange = readout

	def _get_exposure_time(self):
		return self._ccd.IntegrationTime
	
	def _set_exposure_time(self, secs):
		self._ccd.IntegrationTime = secs
	
	def _get_cropping_limits(self):
		limits = self._ccd.GetTotalPixelReadoutRange()
		return (limits.StartY, limits.EndY), (limits.StartX, limits.EndX)

	def _get_exposure_time_limits(self):
		time_range = self._ccd.GetIntegrationTimeRange()
		return time_range.Start, time_range.End

	def _get_binning_limits(self):
		com_vector = self._ccd.BinningValues()
		return [com_vector[i] for i in range(com_vector.Count)]

	def meta_set(self):
		return {
			'binning': self._get_binning_limits(),
			'cropping': self._get_cropping_limits(),
			'exposure_time': self._get_exposure_time_limits(),
		}



