
import comtypes
import comtypes.gen.ESVision as esv

class AcquisitionMode(object):
	Continuous = esv.esContinuousAcquire
	Single     = esv.esSingleAcquire

class AcquisitionType(object):
	Normal = esv.esNormalAcquisition
	Bias   = esv.esBiasAcquisition
	Gain   = esv.esGainAcquisition

def Acquisition(com):
	return com.AcquisitionManager().QueryInterface(esv.IAcquisitionManager)

