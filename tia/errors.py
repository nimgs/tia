
class InterfaceError(Exception):
	pass

class SettingError(Exception):
	pass

class UnknownError(Exception):
	pass

def map(comerror):
	msg = comerror.details[0]
	mapping = {
		'Display object has been deleted': InterfaceError,
		'Invalid binning.': SettingsError
	}
	return mapping.get(msg, default=UnknownError)(msg)


