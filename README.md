# Project Nigel

The purpose of SBIR Phase I proposal is to demonstrate the feasibility of developing software that significantly improves automated data acquisition speed by making better use of hardware, and by allowing imaging of multiplexed sample grids.

* Improve data acquisition efficiency
* Automate imaging of multiplexed grids and create a web-based interface
* Testing and validation

This repository contains code used in part to develop a prototype for Project Nigel. The code was developed by Craig Yoshioka, Lead R&D Developer/Scientist of NanoImaing Services with the assistance of Rise Riyo, R&D Developer.

# Description: _tia_

_tia_ is a Python library that implements a driver for detectors that use the TIA COM interface.

# Dependencies

_tia_ contains as dependencies: _comtypes_, _numpy_, and _enum_.

* _comtypes_ is a lightweight Python COM package, based on the ctypes FFI library
* _numpy_ is a general-purpose array-processing package designed to efficiently manipulate large multi-dimensional arrays
* _enum_ provides for robust enumerations in Python.

Note: See Installation

# Documentation

Not available yet.

# Installation

One can install _tia_ in the following way:

If [Anaconda](www.continuum.io) or [Miniconda](www.continuum.io) is already installed, use `conda` to install _tia_.

        conda install --channel https://conda.binstar.org/nigel tia

Note: Installation of _tia_ via `conda` will install the dependencies of _tia_ listed above as well.
